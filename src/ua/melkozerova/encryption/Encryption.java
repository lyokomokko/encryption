package ua.melkozerova.encryption;

public interface Encryption {
    public String encrypt(String text);
    public String decrypt(String text);
}
