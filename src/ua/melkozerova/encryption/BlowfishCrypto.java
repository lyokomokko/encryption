package ua.melkozerova.encryption;

public class BlowfishCrypto implements Encryption {

    @Override
    public String encrypt(String text) {
        int code;
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            code = Math.round((float) Math.random() * 8 + 1);
            result.append(code).append(Integer.toHexString(((int) text.charAt(i)) ^ code)).append("-");
        }
        return result.substring(0, result.lastIndexOf("-"));
    }

    @Override
    public String decrypt(String text) {
        text = text.replace("-", "");
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < text.length(); i += 3) {
            String hex = text.substring(i + 1, i + 3);
            result.append((char) (Integer.parseInt(hex, 16) ^ (Integer.parseInt(String.valueOf(text.charAt(i))))));
        }
        return result.toString();
    }
}
