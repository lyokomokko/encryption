package ua.melkozerova.encryption;

public class EncryptionBuilder {

    private String text;
    private boolean encryption;
    private boolean cezar = false;
    private boolean reverse = false;
    private boolean blowfish = false;

    public EncryptionBuilder(String text, boolean encryption) {
        this.text = text;
        this.encryption = encryption;
    }

    public EncryptionBuilder enableCezar() {
        this.cezar = true;
        return this;
    }

    public EncryptionBuilder enableBlowfish() {
        this.blowfish = true;
        return this;
    }

    public EncryptionBuilder enableReverse() {
        this.reverse = true;
        return this;
    }

    public String build() {
        if (encryption)
            return encrypt();
        else
            return decrypt();
    }

    private String encrypt() {
        if (cezar)
            text = new CezarCrypto().encrypt(text);
        if (reverse)
            text = new ReverseCrypto().encrypt(text);
        if (blowfish)
            text = new BlowfishCrypto().encrypt(text);
        return text;
    }

    private String decrypt() {
        if (cezar)
            text = new BlowfishCrypto().decrypt(text);
        if (reverse)
            text = new ReverseCrypto().decrypt(text);
        if (blowfish)
            text = new CezarCrypto().decrypt(text);
        return text;
    }
}
