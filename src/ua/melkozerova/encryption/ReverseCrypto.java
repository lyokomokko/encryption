package ua.melkozerova.encryption;

public class ReverseCrypto implements Encryption {

    @Override
    public String encrypt(String text) {
        StringBuilder a = new StringBuilder(text);
        return a.reverse().toString();
    }

    @Override
    public String decrypt(String text) {
        return encrypt(text);
    }
}
