package ua.melkozerova;

import ua.melkozerova.encryption.*;

public class Main {

    public static void main(String[] args) {
        final String myString = "SomeSecretString";

        encryptWithLog("Cezar", new CezarCrypto(), myString);
        encryptWithLog("Blowfish", new BlowfishCrypto(), myString);
        encryptWithLog("Reverse", new ReverseCrypto(), myString);

        String enc = new EncryptionBuilder(myString, true).enableCezar().enableBlowfish().enableReverse().build();
        System.out.println("Encrypted: " + enc);
        String dec = new EncryptionBuilder(enc, false).enableBlowfish().enableCezar().enableReverse().build();
        System.out.println("Decrypted: " + dec);
    }

    private static void encryptWithLog(String encryptionName, Encryption encryption, String text) {
        String encryptedString = encryption.encrypt(text);
        System.out.println(encryptionName + " encryption: " + encryptedString);
        System.out.println(encryptionName + " decryption: " + encryption.decrypt(encryptedString));
    }
}
